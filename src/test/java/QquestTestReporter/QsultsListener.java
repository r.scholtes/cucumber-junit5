package QquestTestReporter;

import org.apache.hc.core5.http.io.entity.StringEntity;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.engine.TestTag;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class QsultsListener implements TestExecutionListener {
    List<Map<String, Object>> cases = new ArrayList<>();
    Map<String, Long> startTimes = new HashMap<>();
    Boolean disabled = false;
    String config = System.getProperty("QsultsConfig");
    String target = System.getProperty("QsultsTarget");
    String files = System.getProperty("QsultsFiles");
    Boolean nosuites = System.getProperty("QsultsNoSuites") != null;
    String buildName = System.getProperty("QsultsBuildName");
    String buildDesc = System.getProperty("QsultsBuildDesc");
    String buildResult = System.getProperty("QsultsBuildResult");
    String buildReason = System.getProperty("QsultsBuildReason");

    public QsultsListener() {
    }

    public void testPlanExecutionStarted(TestPlan testPlan) {
        if (this.target == null) {
            System.out.println("Qsults disabled - target not provided.");
            this.disabled = true;
        } else {
            if (this.config != null) {
                FileInputStream in = null;

                try {
                    //load properties from Qsults.properties file
                    Properties props = new Properties();
                    in = new FileInputStream(System.getProperty("QsultsConfig"));
                    props.load(in);
                    if (props.getProperty(this.target, (String)null) != null) {
                        this.target = props.getProperty(this.target);
                        if (this.target.equals("")) {
                            System.out.println("Invalid target value in configuration file");
                        }
                    }

                    if (this.files == null) {
                        this.files = props.getProperty("QsultsFiles", (String)null);
                    }

                    if (!this.nosuites) {
                        String nosuitesConfig = props.getProperty("QsultsNoSuites", (String)null);
                        if (nosuitesConfig != null && nosuitesConfig.toLowerCase().equals("true")) {
                            this.nosuites = true;
                        }
                    }

                    if (this.buildName == null) {
                        this.buildName = props.getProperty("QsultsBuildName", (String)null);
                    }

                    if (this.buildDesc == null) {
                        this.buildDesc = props.getProperty("QsultsBuildDesc", (String)null);
                    }

                    if (this.buildResult == null) {
                        this.buildResult = props.getProperty("QsultsBuildResult", (String)null);
                    }

                    if (this.buildReason == null) {
                        this.buildReason = props.getProperty("QsultsBuildReason", (String)null);
                    }
                } catch (FileNotFoundException var15) {
                    System.out.println("Configuration file specified for Qsults not found");
                } catch (IOException var16) {
                    var16.printStackTrace();
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException var14) {
                            var14.printStackTrace();
                        }
                    }

                }
            }

        }
    }

    public void executionStarted(TestIdentifier testIdentifier) {
        if (!this.disabled) {
            if (testIdentifier.isTest()) {
                this.startTimes.put(testIdentifier.getUniqueId(), System.currentTimeMillis());
            }

        }
    }

    public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) {
        if (!this.disabled) {
            //validate whether the testIdentifier is a Test type instead of a Container type
            if (testIdentifier.isTest()) {
                String result = testExecutionResult.getStatus().toString();
                if (result.equals("SUCCESSFUL")) {
                    result = "pass";
                } else if (result.equals("FAILED")) {
                    result = "fail";
                } else {
                    result = "unknown";
                }

                String reason = "";
                if (testExecutionResult.getThrowable().isPresent()) {
                    reason = ((Throwable)testExecutionResult.getThrowable().get()).getMessage();
                }

                String suite = null;
                String name = testIdentifier.getDisplayName();
                int index = name.lastIndexOf("()");
                if (index > -1) {
                    name = name.substring(0, index);
                }

                Map<String, Object> testCase = new HashMap<>();
                testCase.put("name", name);
                testCase.put("result", result);
                testCase.put("reason", reason);
                if (this.startTimes.get(testIdentifier.getUniqueId()) != null) {
                    testCase.put("start", this.startTimes.get(testIdentifier.getUniqueId()));
                }

                testCase.put("end", System.currentTimeMillis());
                Iterator var9 = testIdentifier.getTags().iterator();

                while(var9.hasNext()) {
                    TestTag tag = (TestTag)var9.next();
                    String tagVal = tag.getName();
                    if (tagVal.indexOf("desc=") == 0) {
                        testCase.put("desc", tagVal.substring(5));
                    }

                    int indexUnderScore = tagVal.indexOf("_");
                    int indexEquals = tagVal.indexOf("=");
                    if (indexUnderScore == 0 && indexEquals > 0 && indexEquals < tagVal.length()) {
                        String customName = tagVal.substring(0, indexEquals);
                        String customValue = tagVal.substring(indexEquals + 1);
                        testCase.put(customName, customValue);
                    }

                    //extract suite
                    if (tagVal.indexOf("suite=") == 0) {
                        suite = tagVal.substring(6);
                        suite = suite.replace("_", " ");
                        testCase.put("suite", suite);
                    }
                }

                if (suite == null && !this.nosuites) {
                    String separator = "class:";
                    if (testIdentifier.getParentId().isPresent()) {
                        suite = (String)testIdentifier.getParentId().get();
                        if (!suite.contains("/[test-template:")) {
                            suite = suite.substring(suite.indexOf(separator) + separator.length(), suite.lastIndexOf("]"));
                            index = suite.lastIndexOf(46);
                            if (index > -1) {
                                suite = suite.substring(index + 1);
                            }
                        } else {
                            suite = suite.substring(0, suite.indexOf("/[test-template:"));
                            suite = suite.substring(suite.indexOf(separator) + separator.length(), suite.lastIndexOf("]"));
                            index = suite.lastIndexOf(46);
                            if (index > -1) {
                                suite = suite.substring(index + 1);
                            }
                        }
                    }

                    testCase.put("suite", suite);
                }

                List<String> files = this.filesForCase(suite == null ? "" : suite, name);
                if (files != null && files.size() > 0) {
                    testCase.put("files", files);
                }

                this.cases.add(testCase);
                System.out.println("testCase" + testCase);
            }

        }
    }

    public void testPlanExecutionFinished(TestPlan testPlan) {
        if (!this.disabled) {
            HashMap buildCase;
            if (this.buildName != null) {
                if (this.buildName.equals("")) {
                    this.buildName = "-";
                }

                buildCase = new HashMap();
                buildCase.put("name", this.buildName);
                buildCase.put("suite", "[build]");
                if (this.buildDesc != null) {
                    if (this.buildDesc.equals("")) {
                        this.buildDesc = "-";
                    }

                    buildCase.put("desc", this.buildDesc);
                }

                if (this.buildReason != null) {
                    if (this.buildReason.equals("")) {
                        this.buildReason = "-";
                    }

                    buildCase.put("reason", this.buildReason);
                }

                if (this.buildResult != null) {
                    if (this.buildResult.toLowerCase().equals("pass")) {
                        buildCase.put("result", "pass");
                    } else if (this.buildResult.toLowerCase().equals("fail")) {
                        buildCase.put("result", "fail");
                    } else {
                        buildCase.put("result", "unknown");
                    }
                } else {
                    buildCase.put("result", "unknown");
                }

                List<String> files = this.filesForCase("[build]", this.buildName);
                if (files != null && files.size() > 0) {
                    buildCase.put("files", files);
                }

                this.cases.add(buildCase);
            }

            buildCase = new HashMap<>();
            buildCase.put("target", this.target);
            Map<String, Object> results = new HashMap<>();
            results.put("cases", this.cases);
            buildCase.put("results", results);
            System.out.println("Qsults results upload...");
            System.out.println(results);
            Map<String, Object> response = QResults.upload(buildCase);
            System.out.println("success: " + response.get("success"));
            System.out.println("message: " + response.get("message"));
            System.out.println("warnings: " + ((List)response.get("warnings")).size());
            System.out.println("errors: " + ((List)response.get("errors")).size());

//    System.out.println(response);
//
//            String success = "" + response.get("success");
//            String message = "" + response.get("message");
//            int warnings = Integer.parseInt("" + ((List)response.get("warnings")).size());
//            int errors = Integer.parseInt(""+ ((List)response.get("errors")).size());
//
//            //connect to api
//            HttpClient httpclient = HttpClients.createDefault();
//            HttpPost httppost = new HttpPost("https://qquest.mendixcloud.com/rest/bugsweeperdb/v1/highscore?=");
//
//            // Request parameters and other properties.
//            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
//            params.add(new BasicNameValuePair("naam", "Pedro"));
//            params.add(new BasicNameValuePair("tijd", Integer.toString(7)));
//            params.add(new BasicNameValuePair("niveau", "Test"));
//            httppost.setEntity(new UrlEncodedFormEntity( (List)response, StandardCharsets.UTF_8));
//
//
//
//            //Execute and get the response.
//            try{
//                HttpResponse reply = httpclient.execute(httppost);
//                HttpEntity entity = reply.getEntity();
//                if (entity != null) {
//                    try (InputStream instream = entity.getContent()) {
//                        System.out.println(entity);
//                    }
//                }
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            }





//            try {
//                //Creating Connection Object
//                Connection connection= DriverManager.getConnection("jdbc:postgresql://localhost:5432/newdb","postgres","password");
//                //Preapared Statement
//                PreparedStatement ps =connection.prepareStatement("INSERT INTO testresult (success, message, warnings, errors) VALUES (?, ?, ?, ?)");
//                //Specifying the values of it's parameter
//                ps.setString(1, success);
//                ps.setString(2, message);
//                ps.setInt(3, warnings);
//                ps.setInt(4, errors);
//
//                //Execute the query
//                ps.executeUpdate();
//
//
//            } catch (SQLException e1) {
//                e1.printStackTrace();
//            }
//
        }
    }

    public List<String> filesForCase(String suite, String name) {
        if (this.files == null) { //files is a json object, it is always null with the examples we currently use
            return null;
        } else {
            List<String> caseFiles = new ArrayList<>();
            String pathString = Paths.get(this.files, name).toString();
            System.out.println(pathString);
            if (!suite.equals("") && suite != null) {
                pathString = Paths.get(this.files, suite, name).toString();
            }

            File path = new File(pathString);

            try {
                File[] files = path.listFiles();
                File[] var7 = files;
                int var8 = files.length;

                for(int var9 = 0; var9 < var8; ++var9) {
                    File file = var7[var9];
                    if (!file.isDirectory() && !file.getName().equals(".DS_Store")) {
                        caseFiles.add(file.getAbsolutePath());
                    }
                }
            }

            catch (NullPointerException var11) {
            } catch (Exception var12) {
                System.out.println("Exception1: " + var12.toString());
            }

            return caseFiles;
        }
    }
}
