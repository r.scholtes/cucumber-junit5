//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package QquestTestReporter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;




import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class QResults {
    static final String QsultsUrl = "https://qquest.mendixcloud.com";
    static final long expireBuffer = 30L;
    public QResults() {
    }


    private static Map<String, Object> uploadResult(Boolean success, String message, List<String> warnings) {
        HashMap<String, Object> uploadResultMap = new HashMap<>();
        List<String> errors = new ArrayList<>();
        uploadResultMap.put("success", success);
        uploadResultMap.put("message", message);
        if (!success) {
            errors.add(message);
        }

        uploadResultMap.put("warnings", warnings);
        uploadResultMap.put("errors", errors);
        return uploadResultMap;
    }


    public static Map<String, Object> upload(Map<String, Object> data) {
        List<String> warnings = new ArrayList<>();

        String jsonData;
        try {
            jsonData = (new ObjectMapper()).writeValueAsString(data);
        } catch (JsonProcessingException var27) {
            return uploadResult(false, "Incorrect data format.", warnings);
        }

        URL url;
        try {
            url = new URL("https://tickettonite-sandbox.mxapps.io/rest/qualitymonitor/v1/testcase");
        } catch (MalformedURLException var26) {
            return uploadResult(false, "Incorrect url.", warnings);
        }

        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
        } catch (IOException var25) {
            return uploadResult(false, "Unable to connect.", warnings);
        }

        String token = "QQUEST-TST";
        Properties props = new Properties();
        try {
            props.load(Files.newInputStream(Paths.get(System.getProperty("QsultsConfig"))));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (props.getProperty(token, (String)null) != null) {
            token = props.getProperty(token);
        }

        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("Authorization", "Bearer " + token);


        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        connection.setRequestProperty("Authorization", "Bearer " + token);


        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException var24) {
            return uploadResult(false, "Bad request method.", warnings);
        }

        OutputStream os;
        try {
            os = connection.getOutputStream();
        } catch (IOException var23) {
            return uploadResult(false, "Bad response.", warnings);
        }

        try {
            os.write(jsonData.getBytes(StandardCharsets.UTF_8));
        } catch (UnsupportedEncodingException var21) {
            return uploadResult(false, "Bad encoding.", warnings);
        } catch (IOException var22) {
            return uploadResult(false, "Unable to write data.", warnings);
        }

        try {
            os.close();
        } catch (IOException var20) {
            return uploadResult(false, "Unable to close connection.", warnings);
        }

        boolean success = false;

        JSONObject jsonObject;
        String message;
        try {
            BufferedInputStream in;
            if (connection.getResponseCode() == 201) {
                success = true;
                in = new BufferedInputStream(connection.getInputStream());
            } else {
                in = new BufferedInputStream(connection.getErrorStream());
            }

            message = IOUtils.toString(in, StandardCharsets.UTF_8);
            jsonObject = new JSONObject(message);
            in.close();
        } catch (IOException var19) {
            return uploadResult(false, "Error processing response.", warnings);
        }

        if (!success) {
            message = (String)jsonObject.getJSONObject("error").get("message");
            connection.disconnect();
            return uploadResult(false, message, warnings);
        } else {
            JSONObject successData = jsonObject.getJSONObject("data");
            message = (String) successData.get("message");
            if (successData.has("upload")) {
                String target = (String)data.get("target");
//                List<ResultsFile> files = filesInTestCases(data);
                JSONObject upload = successData.getJSONObject("upload");
                String key = (String)upload.get("key");
                String uploadMessage = (String)upload.get("message");
                Boolean permit = (Boolean)upload.get("permit");
                JSONObject auth = upload.getJSONObject("auth");
//                if (!permit) {
                warnings.add(uploadMessage);
                connection.disconnect();
                return uploadResult(true, message, warnings);
//                }
//                else {
//                    FileUploadReturn fileUploadReturn = filesUpload(files, key, auth, target);
//                    connection.disconnect();
//                    return uploadResult(true, fileUploadReturn.message, fileUploadReturn.warnings);
//                }
            }
            else {
                return uploadResult(true, message, warnings);
            }
        }
    }

    private static class ResultsFile {
        public int num;
        public File file;

        private ResultsFile() {
        }
    }

    private static class FileUploadReturn {
        public String message;
        public List<String> warnings;

        private FileUploadReturn() {
        }
    }

    private static class RefreshCredentialsResponse {
        public Boolean success;
        public String message;
        public JSONObject upload;

        private RefreshCredentialsResponse() {
        }
    }
}
