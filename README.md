# Cucumber Junit5

Het doel van dit project is een tool ontwikkelen die de resultaten van een geautomatiseerde test output naar een Mendix applicatie.

De output van testresultaten kan je [hier vinden](https://tickettonite-sandbox.mxapps.io/). Switch met het donkerblauwe icoontje rechts van het scherm naar de administrator en klik dan in de balk linksboven op Testcases. Hier zie je een lijst met testresultaten.

Je runt de code door de klasse CucumberTest te runnen in src/test/runners/CucumberTest.java